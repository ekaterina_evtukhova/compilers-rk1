package com.evtukhova.entity;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Екатерина on 02.03.2017.
 */
public class Statements {
    private String taskName;
    private List<Equivalence> equivalences = new ArrayList<>();
    private List<Equivalence> queries = new ArrayList<>();

    public List<Equivalence> getEquivalences() {
        return equivalences;
    }

    public void setEquivalences(List<Equivalence> equivalences) {
        this.equivalences = equivalences;
    }

    public List<Equivalence> getQueries() {
        return queries;
    }

    public void setQueries(List<Equivalence> queries) {
        this.queries = queries;
    }

    public void addEquivalences(Equivalence equivalence) {
        this.equivalences.add(equivalence);
    }

    public void addQueries(Equivalence query) {
        this.queries.add(query);
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    @Override
    public String toString() {
        DecimalFormat formatter = new DecimalFormat("#0.000");
        System.out.println(taskName);
        for (Equivalence e : equivalences) {
            StringBuilder sb = new StringBuilder();
            sb.append(formatter.format(e.getQ1().getNumber()));
            for (Unit u : e.getQ1().getUnits()) {
                sb.append(" ");
                sb.append(u.getUnitName());
            }
            sb.append(" = ");
            sb.append(formatter.format(e.getQ2().getNumber()));
            for (Unit u : e.getQ2().getUnits()) {
                sb.append(" ");
                sb.append(u.getUnitName());
            }
            System.out.println(sb.toString());
        }
        return null;
    }
}
