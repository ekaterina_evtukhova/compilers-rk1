package com.evtukhova.entity;

/**
 * Created by Екатерина on 02.03.2017.
 */
public abstract class Unit {
    private String unitName;

    protected Unit(String unitName) {
        this.unitName = unitName;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    @Override
    public int hashCode() {
        return unitName.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (!o.getClass().equals(this.getClass()))
            return false;
        if (((Unit)o).getUnitName().equals(this.getUnitName()))
            return true;
        return false;
    }
}
