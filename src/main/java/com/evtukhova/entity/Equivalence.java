package com.evtukhova.entity;

/**
 * Created by Екатерина on 02.03.2017.
 */
public class Equivalence {
    private Quantity q1;
    private Quantity q2;

    public Quantity getQ1() {
        return q1;
    }

    public void setQ1(Quantity q1) {
        this.q1 = q1;
    }

    public Quantity getQ2() {
        return q2;
    }

    public void setQ2(Quantity q2) {
        this.q2 = q2;
    }

    @Override
    public int hashCode() {
        return q1.hashCode() + q2.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Equivalence)) {
            return false;
        }

        if (((Equivalence) o).getQ1().equals(this.getQ1()) && ((Equivalence) o).getQ2().equals(this.getQ2()))
            return true;
        return false;
    }
}
