package com.evtukhova.entity;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Екатерина on 02.03.2017.
 */
public class Quantity {
    private Double number;
    private List<Unit> units = new LinkedList<>();

    public Quantity() {
    }

    public Quantity(Quantity q) {
        this.number = q.getNumber();
        this.units = new LinkedList<>(q.getUnits());
    }

    public Double getNumber() {
        return number;
    }

    public void setNumber(Double number) {
        this.number = number;
    }

    public List<Unit> getUnits() {
        return units;
    }

    public void setUnits(List<Unit> units) {
        this.units = units;
    }

    public void addUnit(Unit unit) {
        units.add(unit);
    }

    @Override
    public int hashCode() {
        return number.hashCode() + units.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Quantity))
            return false;
        if (!((Quantity) o).getNumber().equals(this.getNumber()))
            return false;
        if (((Quantity) o).getUnits().size() == this.getUnits().size() &&
                ((Quantity) o).getUnits().containsAll(this.getUnits()))
            return true;
        return false;
    }
}
