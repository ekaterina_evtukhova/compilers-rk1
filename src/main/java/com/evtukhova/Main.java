package com.evtukhova;

import com.evtukhova.entity.Statements;
import com.evtukhova.utils.FileParser;
import com.evtukhova.utils.UnitCalculator;

/**
 * Created by Екатерина on 02.03.2017.
 */
public class Main {

    public static void main(String[] args) {
        Statements statements = FileParser.getInstance().parse(args[0]);
        //System.out.println(statements.toString());
        Statements answers = UnitCalculator.getInstance().calculate(statements);
        answers.toString();
    }
}
