package com.evtukhova.utils;

import com.evtukhova.entity.*;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

/**
 * Created by Екатерина on 02.03.2017.
 */
public class FileParser {
    private static volatile FileParser instance;

    public Statements parse(String fileName) {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource(fileName).getFile());

        Statements statements = new Statements();
        try (Scanner scanner = new Scanner(file)) {
            if (scanner.hasNextLine()) {
                statements.setTaskName(scanner.nextLine());
            }

            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                if (line.equals("."))
                    break;

                Equivalence equivalence = new Equivalence();
                String[] parts = line.split(" = ");
                //left part
                String leftPart = parts[0];
                equivalence.setQ1(buildQuantity(leftPart));
                //right part
                String rightPart = parts[1];
                equivalence.setQ2(buildQuantity(rightPart));
                if (rightPart.charAt(0) == '?') {
                    statements.addQueries(equivalence);
                } else{
                    statements.addEquivalences(equivalence);
                }

            }
        } catch (IOException e) {
            e.printStackTrace();
        }


        return statements;
    }

    private Quantity buildQuantity(String part) {
        String[] quantities = part.split(" ");
        Quantity quantity = new Quantity();
        if (quantities[0].equals("?")) {
            quantity.setNumber(null);
        } else {
            quantity.setNumber(Double.valueOf(quantities[0]));
        }
        boolean wasPrevDiv = false;
        for (int i = 1; i < quantities.length; i++) {
            if (quantities[i].equals("/")) {
                wasPrevDiv = true;
            } else {
                if (wasPrevDiv) {
                    Divisor divisor = new Divisor(quantities[i]);
                    quantity.addUnit(divisor);
                    wasPrevDiv = false;
                } else {
                    UnitName unitName = new UnitName(quantities[i]);
                    quantity.addUnit(unitName);
                }
            }
        }
        return quantity;
    }

    private FileParser() {

    }

    public static synchronized FileParser getInstance() {
        if (instance == null) {
            instance = new FileParser();
        }
        return instance;
    }
}
