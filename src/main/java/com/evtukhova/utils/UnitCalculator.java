package com.evtukhova.utils;

import com.evtukhova.entity.*;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Екатерина on 03.03.2017.
 */
public class UnitCalculator {
    private static volatile UnitCalculator instance;

    private List<Equivalence> preprocess(List<Equivalence> list) {
        List<Equivalence> result = new ArrayList<>(list);
        for (Equivalence e : result) {
            List<Unit> divisorsQ1 = e.getQ1().getUnits().stream()
                    .filter(x -> x instanceof Divisor)
                    .collect(Collectors.toList());
            if (divisorsQ1 !=  null && !divisorsQ1.isEmpty()) {
                e.getQ1().setUnits(e.getQ1().getUnits().stream()
                .filter(x -> !(x instanceof Divisor))
                .collect(Collectors.toList()));

                e.getQ2().setUnits(Stream.concat(divisorsQ1.stream()
                        .map(x -> new UnitName(x.getUnitName())),
                        e.getQ2().getUnits().stream())
                        .collect(Collectors.toList()));
            }

            List<Unit> divisorsQ2 = e.getQ2().getUnits().stream()
                    .filter(x -> x instanceof Divisor)
                    .collect(Collectors.toList());
            if (divisorsQ2 !=  null && !divisorsQ2.isEmpty()) {
                e.getQ2().setUnits(e.getQ2().getUnits().stream()
                        .filter(x -> !(x instanceof Divisor))
                        .collect(Collectors.toList()));

                e.getQ1().setUnits(Stream.concat(divisorsQ2.stream()
                                .map(x -> new UnitName(x.getUnitName())),
                        e.getQ1().getUnits().stream())
                        .collect(Collectors.toList()));
            }
        }
        return result;
    }

    public Statements calculate(Statements s) {
        Statements result = new Statements();
        s.setEquivalences(preprocess(s.getEquivalences()));
        s.setQueries(preprocess(s.getQueries()));
        result.setTaskName(s.getTaskName());

        for (Equivalence e : s.getQueries() ) {
            Equivalence resultEq = new Equivalence();
            resultEq.setQ1(new Quantity(e.getQ1()));

            List<Equivalence> statements = new ArrayList<>(s.getEquivalences());
            Equivalence eResult = calculate3(e, statements);

            resultEq.setQ2(eResult.getQ1());
            result.getEquivalences().add(resultEq);
        }

        return result;
    }

    private Equivalence calculate3(Equivalence query, List<Equivalence> statements) {
        List<Unit> leftPart = query.getQ1().getUnits();
        List<Unit> rightPart = query.getQ2().getUnits();

        if (leftPart.size() == rightPart.size() && leftPart.containsAll(rightPart)) {
            return query;
        }

        for (Equivalence e : statements) {
            if (leftPart.containsAll(e.getQ1().getUnits())) {
                Equivalence newQuery = new Equivalence();
                newQuery.setQ2(new Quantity(query.getQ2()));
                Quantity q1 = new Quantity();
                q1.setUnits(new LinkedList<>(leftPart));
                q1.getUnits().removeAll(e.getQ1().getUnits());
                for (Unit u : e.getQ2().getUnits()) {
                    q1.addUnit(u);
                }
                q1.setNumber(e.getQ2().getNumber() * query.getQ1().getNumber() / e.getQ1().getNumber());
                newQuery.setQ1(q1);
                List<Equivalence> newS = new ArrayList<>(statements);
                newS.remove(e);
                Equivalence e1 = calculate3(newQuery, newS);
                if (e1 != null)
                    return e1;
            }
            if (leftPart.containsAll(e.getQ2().getUnits())) {
                Equivalence newQuery = new Equivalence();
                newQuery.setQ2(new Quantity(query.getQ2()));
                Quantity q1 = new Quantity();
                q1.setUnits(new LinkedList<>(leftPart));
                q1.getUnits().removeAll(e.getQ2().getUnits());
                for (Unit u : e.getQ1().getUnits()) {
                    q1.addUnit(u);
                }
                q1.setNumber(e.getQ1().getNumber() * query.getQ1().getNumber() / e.getQ2().getNumber());
                newQuery.setQ1(q1);
                List<Equivalence> newS = new ArrayList<>(statements);
                newS.remove(e);
                Equivalence e1 = calculate3(newQuery, newS);
                if (e1 != null)
                    return e1;
            }
        }
        return null;
    }

    private Equivalence calculate2(Equivalence query, List<Equivalence> statements){
        List<Unit> leftPart = query.getQ1().getUnits();
        List<Unit> rightPart = query.getQ2().getUnits();

        if (leftPart.equals(rightPart)) {
            return query;
        }

        List<Unit> u1 = leftPart.stream()
                .map(x -> {
                    boolean b = statements.stream()
                        .map(y -> y.getQ1().getUnits().size() == 1 &&
                            y.getQ1().getUnits().contains(x))
                        .reduce(false, (acc, elem) -> acc || elem);
                    return b ? x : null;})
                .filter(x -> x != null)
                .collect(Collectors.toList());
        if (u1 != null && !u1.isEmpty()) {
            Equivalence newQuery = new Equivalence();
            newQuery.setQ2(query.getQ2());
            Double d = query.getQ1().getNumber();
            List<Equivalence> newS = new ArrayList<>(statements);
            for (Unit u : u1) {
                for (Equivalence e : statements) {
                    if (e.getQ1().getUnits().size() == 1 &&
                            e.getQ1().getUnits().contains(u)) {
                        leftPart.remove(u);
                        leftPart.addAll(e.getQ2().getUnits());
                        d = e.getQ2().getNumber() * d / e.getQ1().getNumber();
                        newS.remove(e);
                        Quantity q1 = new Quantity();
                        q1.setUnits(leftPart);
                        q1.setNumber(d);
                        newQuery.setQ1(q1);
                        Equivalence e1 = calculate2(newQuery, newS);
                        if (e1 != null)
                            return e1;
                        newS.add(e);
                    }
                }
//                Equivalence e = statements.stream()
//                        .filter(y -> y.getQ1().getUnits().size() == 1 &&
//                                y.getQ1().getUnits().contains(u))
//                        .collect(Collectors.toList()).get(0);
//                leftPart.remove(u);
//                leftPart.addAll(e.getQ2().getUnits());
//                d = e.getQ2().getNumber() * d / e.getQ1().getNumber();
//                newS.remove(e);
            }
//            Quantity q1 = new Quantity();
//            q1.setUnits(leftPart);
//            q1.setNumber(d);
//            newQuery.setQ1(q1);
//            Equivalence e = calculate2(newQuery, newS);
//            if (e != null)
//                return e;
        }
        {
            List<Unit> u2 = leftPart.stream()
                    .map(x -> {
                        boolean b = statements.stream()
                                .map(y -> y.getQ2().getUnits().size() == 1 &&
                                        y.getQ2().getUnits().contains(x))
                                .reduce(false, (acc, elem) -> acc || elem);
                        return b ? x : null;
                    })
                    .filter(x -> x != null)
                    .collect(Collectors.toList());
            if (u2 != null && !u2.isEmpty()) {
                Equivalence newQuery = new Equivalence();
                newQuery.setQ2(query.getQ2());
                Double d = query.getQ1().getNumber();
                List<Equivalence> newS = new ArrayList<>(statements);
                for (Unit u : u2) {
                    for (Equivalence e : statements) {
                        if (e.getQ2().getUnits().size() == 1 &&
                                e.getQ2().getUnits().contains(u)) {
                            leftPart.remove(u);
                            leftPart.addAll(e.getQ1().getUnits());
                            d = e.getQ1().getNumber() * d / e.getQ2().getNumber();
                            newS.remove(e);
                            Quantity q1 = new Quantity();
                            q1.setUnits(leftPart);
                            q1.setNumber(d);
                            newQuery.setQ1(q1);
                            Equivalence e1 = calculate2(newQuery, newS);
                            if (e1 != null)
                                return e1;
                            newS.add(e);
                        }
                    }
//                    Equivalence e = statements.stream()
//                            .filter(y -> y.getQ2().getUnits().size() == 1 &&
//                                    y.getQ2().getUnits().contains(u))
//                            .collect(Collectors.toList()).get(0);
//                    leftPart.remove(u);
//                    leftPart.addAll(e.getQ1().getUnits());
//                    d = e.getQ1().getNumber() * d / e.getQ2().getNumber();
//                    newS.remove(e);
                }
//                Quantity q1 = new Quantity();
//                q1.setUnits(leftPart);
//                q1.setNumber(d);
//                newQuery.setQ1(q1);
//                Equivalence e = calculate2(newQuery, newS);
//                if (e != null)
//                    return e;
            }
        }
        return null;
    }

    private Equivalence calculate(Equivalence query, List<Equivalence> statements){
        String leftPart = query.getQ1().getUnits().get(0).getUnitName();
        String rightPart = query.getQ2().getUnits().get(0).getUnitName();

        if (leftPart.equals(rightPart)) {
            return query;
        }

        for (Equivalence s : statements) {
            String sLeft = s.getQ1().getUnits().get(0).getUnitName();
            String sRight = s.getQ2().getUnits().get(0).getUnitName();
            if (sLeft.equals(leftPart)) {
                Equivalence newQuery = new Equivalence();
                newQuery.setQ2(query.getQ2());
                Quantity q1 = new Quantity();
                q1.setUnits(s.getQ2().getUnits());
                q1.setNumber(s.getQ2().getNumber()*query.getQ1().getNumber() / s.getQ1().getNumber());
                newQuery.setQ1(q1);

                List<Equivalence> newS = new ArrayList<>(statements);
                newS.remove(s);

                Equivalence e = calculate(newQuery, newS);
                if (e != null)
                    return e;
            } else if (sRight.equals(leftPart)) {
                Equivalence newQuery = new Equivalence();
                newQuery.setQ2(query.getQ2());
                Quantity q1 = new Quantity();
                q1.setUnits(s.getQ1().getUnits());
                q1.setNumber(s.getQ1().getNumber()*query.getQ1().getNumber() / s.getQ2().getNumber());
                newQuery.setQ1(q1);

                List<Equivalence> newS = new ArrayList<>(statements);
                newS.remove(s);

                Equivalence e = calculate(newQuery, newS);
                if (e != null)
                    return e;
            }
        }
        return null;
    }

    private UnitCalculator() {

    }

    public static synchronized UnitCalculator getInstance() {
        if (instance == null) {
            instance = new UnitCalculator();
        }
        return instance;
    }
}
